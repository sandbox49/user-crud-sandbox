@extends('layouts.app')

@section('content')
<h1>User Index</h1>

<hr>

<div class="d-flex justify-content-end pb-3">
    <a href="{{ route('user.create') }}" class="btn btn-sm btn-primary">Create User</a>
</div>

@if (session('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

<ul class="list-group">
    @forelse ($users as $user)
        <li class="list-group-item">
            <a href="{{ route('user.show', $user) }}">
                {{ $user->name }}
            </a>
        </li>
    @empty
        <span class="text-center">No users</span>
    @endforelse
</ul>
@endsection
