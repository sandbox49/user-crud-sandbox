@extends('layouts.app')

@section('content')
<h1>User Create</h1>

<hr>

<form action="{{ route('user.store') }}" method="POST">
    @csrf
    <div class="form-group">
        <input type="text" name="name" placeholder="Name" class="form-control" autofocus required>
    </div>

    <div class="form-group">
        <input type="email" name="email" placeholder="Email" class="form-control" required>
    </div>

    <div class="form-group">
        <input type="password" name="password" placeholder="Password" class="form-control" required>
    </div>

    <div class="form-group">
        <button class="btn btn-sm btn-success" type="submit">Create User</button>
    </div>
</form>
@endsection
