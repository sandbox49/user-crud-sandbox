@extends('layouts.app')

@section('content')
<h1>User Edit</h1>

<hr>

<div class="d-flex justify-content-end mb-3">
    <form action="{{ route('user.destroy', $user) }}" method="POST">
        @csrf
        @method('DELETE')
        <button class="btn btn-sm btn-danger" type="submit">Delete User</button>
    </form>
</div>

<form action="{{ route('user.update', $user) }}" method="POST">
    @csrf
    @method('PATCH')
    <div class="form-group">
        <input type="text" name="name" value="{{ $user->name }}" class="form-control" placeholder="Name" autofocus required>
    </div>

    <div class="form-group">
        <input type="email" name="email" value="{{ $user->email }}" class="form-control" placeholder="Email" required>
    </div>

    <div class="form-group">
        <button class="btn btn-sm btn-primary" type="submit">Update</button>
    </div>
</form>
@endsection
