@extends('layouts.app')

@section('content')
<h1>User Show</h1>

<hr>

@if (session('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

<p>
    <b>Name:</b> {{ $user->name }}
</p>

<p>
    <b>Email:</b> {{ $user->email }}
</p>

<a class="btn btn-success" href="{{ route('user.edit', $user) }}">Edit User</a>

<a class="btn btn-light" href="{{ route('user.index') }}">All Users</a>
@endsection
